
api.map('<Ctrl-/>', '<Alt-s>'); // hotkey must be one keystroke with/without modifier, it can not be a sequence of keystrokes like `gg`.

api.mapkey('<Ctrl-g>', 'Show me the money', function() {
    Front.showPopup('a well-known phrase uttered by characters in the 1996 film Jerry Maguire (Escape to close).');
});

// an example to replace `u` with `?`, click `Default mappings` to see how `u` works.
//map('?', 'u');

// an example to remove api.mapkey `Ctrl-i`
// unmap('<Ctrl-i>');

// click `Save` button to make above settings to take effect.
// set theme
settings.theme = '\
.sk_theme { \
    background: #fff; \
    color: #000; \
} \
.sk_theme tbody { \
    color: #000; \
} \
.sk_theme input { \
    color: #000; \
} \
.sk_theme .url { \
    color: #555; \
} \
.sk_theme .annotation { \
    color: #555; \
} \
.sk_theme .focused { \
    background: #f0f0f0; \
}';

api.map('F', 'af');

api.mapkey('<Ctrl-[>', 'fire escape event', function() {
   $('input').blur();
});
    
api.mapkey('<Ctrl-[>', 'fire escape event', function() {
   $('input').blur();
});

api.map('J','E');
api.map('K','R');

api.map('u','e');

api.removeSearchAlias('w');
api.addSearchAlias('lg', 'search in google maps', 'https://www.google.com/maps/search/');
api.addSearchAlias('ff', 'forvo in french', 'https://fr.forvo.com/search/');
api.addSearchAlias('lr', 'larousse', 'https://www.larousse.fr/dictionnaires/francais/');
api.addSearchAlias('fe', 'forvo in english', 'https://forvo.com/search/');
api.addSearchAlias('fc', 'forvo in czech', 'https://cs.forvo.com/search/');
api.addSearchAlias('cf', 'leconjugeur', 'https://leconjugueur.lefigaro.fr/conjugaison/verbe/');
api.addSearchAlias('cr', 'wikiru', 'https://ru.wiktionary.org/wiki/');
api.addSearchAlias('h', 'hungarian-hungarian', 'http://mek.oszk.hu/adatbazis/magyar-nyelv-ertelmezo-szotara/kereses.php?kereses=');
api.addSearchAlias('we', 'english wikipedia', 'https://en.wikipedia.org/wiki/');
api.addSearchAlias('wf', 'french wikipedia', 'https://fr.wikipedia.org/wiki/');
api.addSearchAlias('f', 'forvo in french', 'https://fr.forvo.com/search/');
api.addSearchAlias('c', 'leconjugeur', 'https://leconjugueur.lefigaro.fr/conjugaison/verbe/');
api.addSearchAlias('y', 'youtube', 'https://www.youtube.com/results?search_query=');
api.mapkey('of', '#8Open Search with alias f', function() { Front.openOmnibar({type: "SearchEngine", extra: "f"}); });
api.mapkey('olr', '#8Open Search with alias lr', function() { Front.openOmnibar({type: "SearchEngine", extra: "lr"}); });


api.mapkey('yY', '#1Copy all tabs url', function() {
	runtime.command({action: 'getTabs'}, function (response) {
	 Clipboard.write(response.tabs.map(tab => tab.url).join('\n'));
	});
});

api.mapkey(',yg', '#7Copy a image source URL to the clipboard', function() {
    Hints.create('*[src]', function(element) {
        Clipboard.write(element.src);
    })
});

api.mapkey('yna', '#7yank all links on page', function() {
  var linksToYank = [];
  var links = document.getElementsByTagName("a");
  for(var i=0; i<links.length; i++) {
        linksToYank.push(links[i].href);
  }
  Clipboard.write(linksToYank.join('\n'));
});
api.mapkey('yni', '#7yank all images on page', function() {
  var linksToYank = [];
  var links = document.getElementsByTagName("img");
  for(var i=0; i<links.length; i++) {
    if(links[i]['naturalWidth'] > 400) {
      linksToYank.push(links[i].src);
    }
  }
  linksToYank = linksToYank.filter((value, index, self) => self.indexOf(value) === index);
  Clipboard.write(linksToYank.join('\n'));
});


api.mapkey('cle', 'change language to english on wikipedia', function() {
  if(window.location.href.match(/.*google/)){
    window.location.href = window.location.href + "&hl=en";
  }
  var languageLink = document.querySelector("a.interlanguage-link-target[lang='en']")
  if( languageLink.onclick) {
      var def = languageLink.onclick();
      if( !def) return false; // event cancelled by handler
  }
  window.location.href = languageLink.getAttribute("href");
});

api.mapkey('clf', 'change language to french on wikipedia', function() {
  if(window.location.href.match(/.*google/)){
    window.location.href = window.location.href + "&hl=fr";
  }
  var languageLink = document.querySelector("a.interlanguage-link-target[lang='fr']")
  if( languageLink.onclick) {
      var def = languageLink.onclick();
      if( !def) return false; // event cancelled by handler
  }
  window.location.href = languageLink.getAttribute("href");
});

api.mapkey('clr', 'change language to russian on wikipedia', function() {
  var languageLink = document.querySelector("a.interlanguage-link-target[lang='ru']")
  if( languageLink.onclick) {
      var def = languageLink.onclick();
      if( !def) return false; // event cancelled by handler
  }
  window.location.href = languageLink.getAttribute("href");
});

api.mapkey(',mm', 'refresh', function() {
  setInterval(function() { location.reload(); }, 1000);
});

api.mapkey('clh', 'change language to hungarian on wikipedia', function() {
  var languageLink = document.querySelector("a.interlanguage-link-target[lang='hu']")
  if( languageLink.onclick) {
      var def = languageLink.onclick();
      if( !def) return false; // event cancelled by handler
  }
  window.location.href = languageLink.getAttribute("href");
});

settings.smoothScroll = true;
settings.scrollStepSize = 180;

settings.tabsThreshold = 3;

settings.hintAlign = "left";

settings.stealFocusOnLoad = "true";



